import java.util.*;

public class DoubleStack {
   private LinkedList<Double> stack = new LinkedList<Double>();

   @Override
   public Object clone() throws CloneNotSupportedException {
      LinkedList<Double> clone = (LinkedList<Double>) stack.clone();
      DoubleStack newDoubleStack = new DoubleStack();
      newDoubleStack.setStack(clone);
      return newDoubleStack;
   }
   public void setStack(LinkedList<Double> stack) {
      this.stack = stack;
   }

   public boolean stEmpty() {
      return stack.size() == 0;
   }

   public void push (double a) {
      stack.addLast(a);
   }

   public double pop() {
      if (stack.size() == 0) {
         throw new RuntimeException("Can't pop from empty Stack.");
      }
      return stack.removeLast();
   }

   public void op (String s) {
      if (!isOperator(s)) {
         throw new RuntimeException("Invalid operator: " + s + ".");
      }
      switch (s) {
         case "DUP":
            if (stEmpty()) {
               throw new RuntimeException("Can't execute " + s + " on an empty stack.");
            }
            stack.addLast(stack.getLast());
            return;
         case "SWAP":
            if (stack.size() < 2) {
               throw new RuntimeException("Can't execute " + s + " on a stack with less than 2 elements.");
            }
            stack.addLast(stack.remove(stack.size() - 2));
            return;
         case "ROT":
            if (stack.size() < 3) {
               throw new RuntimeException("Can't execute " + s + " on a stack with less than 3 elements.");
            }
            stack.addLast(stack.remove(stack.size() - 3));
            return;
      }

      if (stack.size() < 2) {
         throw new RuntimeException("Not enough elements in Stack for operation: " + s + ".");
      }

      double topElement = stack.removeLast();
      double top2Element = stack.removeLast();
      double result = 0;

      switch (s) {
         case "+":
            result = top2Element + topElement;
            break;
         case "-":
            result = top2Element - topElement;
            break;
         case "*":
            result = top2Element * topElement;
            break;
         case "/":
            result = top2Element / topElement;
            break;
      }

      stack.addLast(result);
   }
  
   public double tos() {
      if (stEmpty()) {
         throw new RuntimeException("No elements in stack for Tos operation.");
      }
      return stack.getLast();
   }

   @Override
   public boolean equals (Object o) {
      if (o instanceof DoubleStack) {
         DoubleStack otherStack = (DoubleStack)o;
         if (otherStack.stack.size() != stack.size()) {
            return false;
         }

         for (int i = 0; i < stack.size(); i++) {
            if (!Objects.equals(stack.get(i), otherStack.stack.get(i))) {
               return false;
            }
         }
         return true;
      }
      return false;
   }

   @Override
   public String toString() {
      StringBuilder result = new StringBuilder();

      for (int i = 0; i < stack.size(); i++) {
         result.append(stack.get(i));
         if (i != stack.size() - 1) {
            result.append(" ");
         }
      }
      return result.toString();
   }

   public static double interpret (String pol) {
      try {
         if (Objects.equals(pol, "") || pol == null || pol.trim().equals("")) {
            throw new RuntimeException("Can't interpret provided equation as it's invalid.");
         }

         String[] elements = pol.split("\\s+"); // not perfect?

         DoubleStack doubleStack = new DoubleStack();

         for (String element : elements) {
            element = element.trim();

            // skip if wasn't caught by regex
            if (Objects.equals(element, "")) {
               continue;
            }

            if (isOperator(element)) {
               doubleStack.op(element);
            } else {
               try {
                  doubleStack.push(Double.parseDouble(element));
               } catch (NumberFormatException e) {
                  throw new RuntimeException("Invalid character: " + element + ".");
               }
            }
         }

         if (doubleStack.stack.size() > 1) {
            throw new RuntimeException("Not enough operators in equation.");
         }

         return doubleStack.tos();
      } catch (Exception e) {
         throw new RuntimeException(e.getMessage() + " Provided equation: " + pol);
      }
   }
   private static boolean isOperator(String element) {
      List<String> operators = new ArrayList<>();
      operators.add("+");
      operators.add("-");
      operators.add("*");
      operators.add("/");
      operators.add("DUP");
      operators.add("SWAP");
      operators.add("ROT");

      return operators.contains(element);
   }

}

